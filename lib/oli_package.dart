import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'plug_page.dart';

class OliPlugs {
  static const MethodChannel _channel = MethodChannel('oli_phone_contacts');

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Widget getPlug() {
    return PlugPage();
  }
}
